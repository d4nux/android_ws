package com.ebuhoo.d4nux.wschaira;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.Console;

public class MainActivity extends ActionBarActivity {

    private static final String accionSoap = "http://tempuri.org/IServiceChaira/iniciar_sesion";
    private static final String metodo = "iniciar_sesion";
    private static final String namespace = "http://tempuri.org/";
    private static final String url = "http://ebuhoo.com/chairaws/ServiceChaira.svc?wsdl";

    private Button btnNormal;
    private TextView txt;


    private final String user_id= "e.cordoba";
    private final String password= "eliana";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt = (TextView) findViewById(R.id.txtSalida);
        btnNormal = (Button) findViewById(R.id.btnInvocar);

        btnNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt.setText("Cargando datos...");
                thread.start();
            }
        });
    }

    Thread thread = new Thread(new Runnable(){
        @Override
        public void run() {
            try {
                SoapObject request = new SoapObject(namespace, metodo);
                request.addProperty("login", user_id);
                request.addProperty("psw",password);

                SoapSerializationEnvelope sobre = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                sobre.dotNet = true;
                sobre.setOutputSoapObject(request);
                sobre.implicitTypes = true;

                HttpTransportSE http = new HttpTransportSE(url);
                http.debug = true;
                http.call(accionSoap, sobre);

                final SoapPrimitive resul = (SoapPrimitive) sobre.getResponse();

                txt.post(new Runnable() {
                    @Override
                    public void run() {
                        txt.setText(resul.toString());
                    }
                });

            } catch (Exception e) {
                Log.d("Error:", e.toString());
            }
        }
    });

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
